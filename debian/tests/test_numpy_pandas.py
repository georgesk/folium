#! /usr/bin/python3

import unittest
import numpy as np
import pandas as pd


class TestNumpyPandas(unittest.TestCase):

    def test_coordinates(self):
        from folium.utilities import validate_location
        # Dunkerque's location
        lat = 51.032777
        lon = 2.3769444
        obj = [lat, lon]
        assert validate_location(obj) == [lat, lon]
        obj = tuple([lat, lon])
        assert validate_location(obj) == [lat, lon]
        obj = np.array([lat, lon])
        assert validate_location(obj) == [lat, lon]
        obj = pd.DataFrame.from_dict({"latitude": [lat],"longitude": [lon]})
        assert validate_location(obj) == [lat, lon]
        
if __name__ == "__main__":
    unittest.main()
